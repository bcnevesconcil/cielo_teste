# EDI Parser - read EDI files in JSON

A generic parser for EDI files to tranlate it to JSON files 

### Assumptions:

+ Assuming python is installed on your system.
+ Assuming pip is installed on your system.


Install `concil-edi-parser` on your system using : 

```
pip install concil-edi-parser
```

### Usage
**code example**

    ``
        from concil_edi_parser import EdiParser

        EdiParser.parse(filepath, layout)
    ``

**layout example**

- startswith: Get the begin of the line as a identifier to verify what type of register is in the line of the document

- map: An Array of objects with 3 attributes used to setup the object of the line
    - seq: just a sequence if you need to reorder (not implemented)
    - tamanho: accordingly with the documentation of the EDI document how long in string size is the value that you trying to get.
    - desc: the description of the value you're trying to retrieve, it will become the attribute name for the value in the final object


    [{

        'startswith': '0001',

        'map': [

            {'seq': 0, 'tamanho': 4, 'desc': 'identificador'},

            {'seq': 1, 'tamanho': 8, 'desc': 'data_arquivo'}

        ]

    },{

        'startswith': '0002',

        'map': [

            {'seq': 0, 'tamanho':4, 'desc': 'identificador'}, 

            {'seq': 1, 'tamanho': 9, 'desc': 'valor'}, 

            {'seq': 2, 'tamanho': 15, 'desc': 'bandeira'},
            
            {'seq': 3, 'tamanho': 18, 'desc': 'numero_cartao','casting': {'rule': r'\D', 'cast': 'int'}},
            
        ]

    }]

**EDI file example**

- line 1: Starts with ```0001``` so it'll be caught in the first map rule
- line 1: ```0001``` is the identifier of the transaction as the Header with length equals to four
- line 1: ```20190821``` it's a date with [YYYYMMDD] format with length equals 8
...and so on....


    000120190821V3

    0002000005000     MASTERCARD000004950000000001

    0002000010000           VISA000009900000000001

### Methods

**get_layout**

Params(`layout`, `line`)

it will try to find a match in the begining of the `line` with the attribute startswith from the `layout` example json and get the map rules for the parse
of the line to the json dict.

**get_line_json**

Params(`map_layout`, `line`)

pass every rule on the `map_layout` and then apply a substring rule on the line that get the positions as an array line[`from`:`to`]. Create an attribute on the obj_line dict with the name of the rule description `rule['desc']` and create a similar dictionary as `{rule['desc']:line[from:to]}` for the entire line.

It has a casting handler, you can pass the `regex` you want to apply to that part of the line, `rule['casting']['rule'] = r'/[0-9]/i'`. And you can pass a caster to transform the string in `int` or `float`, lik that `rule['casting']['cast'] = 'int'`


**handle_cast**

Params(`type_cast`, `value`)

hadle the casting rules on the EDI translation, you can cast the EDI string to `int`, `float` or even `string` in unknow cases.

**static parser_file**

Params(`filepath`, `layout`)

you pass the location of the file(`filepath`) to be read and the `layout` from the **layout example** and it'll return a list of dicts containing each line transalted accordingly to the layout map rules.

**static parser_line**

Params(`line`, `layout`)

you pass the `line` of the file you are reading as a string, and the `layout` predefined before, the function will locate the map rules on its own and will return an dictionary of the line translated