import setuptools

with open("README.md", "r") as fh:

    long_description = fh.read()

setuptools.setup(

    name='concil_edi_parser',  

    version='0.0.1',

    author="Bruno Campos",

    author_email="bruno.campos@concil.com.br",

    description="A generic EDI parser",

    long_description=long_description,

    url="https://bcnevesconcil@bitbucket.org/bcnevesconcil/cielo_teste.git",

    packages=setuptools.find_packages(),

    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],

 )
