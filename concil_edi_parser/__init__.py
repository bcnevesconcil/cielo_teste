import re
import datetime
class EdiParser:

    def __init__(self):
        self.runtime = 0

    def get_layout(layout, line):
        for item in layout.get('templates'):
            if line.startswith(item.get('startswith')):
                return item.get('map')
        return None


    def get_line_json(map_layout, line):
        obj_line = {}
        last_idx = 0
        for rule in map_layout:
            obj_line[rule['desc']] = line[last_idx:last_idx+rule['tamanho']]
            if 'casting' in rule and 'rule' in rule['casting']:
                obj_line[rule['desc']] = re.sub(rule['casting']['rule'], '', obj_line[rule['desc']])
            if 'casting' in rule and 'cast' in rule['casting']:
                obj_line[rule['desc']] = EdiParser.handle_cast(type_cast=rule['casting']['cast'], value=obj_line[rule['desc']])
            last_idx += rule['tamanho']
        return obj_line

    def handle_cast(type_cast, value):
        if type_cast == 'int':
            return int(value)
        if type_cast == 'float':
            return float(value)
        if type_cast == 'string':
            return str(value)


    @staticmethod
    def parser_file(filepath, layout):
        edi_file = []
        EdiParser.runtime = datetime.datetime.now()
        with open(filepath, 'r') as f:
            for line in f:
                layout_line = EdiParser.get_layout(layout=layout, line=line)
                if layout_line is not None:
                    edi_file.append(EdiParser.get_line_json(map_layout=layout_line, line=line))
        EdiParser.runtime = EdiParser.runtime - datetime.datetime.now()
        return edi_file
    '''

    '''
    @staticmethod
    def parser_line(line, layout):
        layout_line = EdiParser.get_layout(layout=layout, line=line)
        if layout_line is not None:
            return EdiParser.get_line_json(map_layout=layout_line, line=line)
        
        return None